async function leerJSON(url) {

  try {
    let response = await fetch(url);
    let user = await response.json();
    return user;
  } catch(err) {
    
    alert(err);
  }
}


function mostrar()
{
var url="https://raw.githubusercontent.com/madarme/persistencia/main/pizza.json";
var resultado=document.getElementById("nombrepizzeria");
var msg="";
//resultado.innerHTML=msg;

leerJSON(url).then(datos=>{


console.log(datos);

msg+="<h1>"+datos.nombrePizzeria+"</h1>";

//var adicional=datos.adicional;
//var pizzas=datos.pizzas;



resultado.innerHTML=msg;
})

}

function definirTamanos(){
  var numero=document.getElementById("entrada").value;
  var label="";
  var select="";
  var div="";
  var rtas="";
  var btnsalida= "<input class='rounded border-danger' type='button' id='btnsalida' onclick='location.reload()' value='Cargar de nuevo'>";
  var btnOpciones= "<input class='rounded border-danger' type='button' id='btnOpciones' onclick='cargarOpciones("+numero+")' value='Cargar Opciones'>";
  var espacioNumero= document.getElementById("entrada");
  var btnNumero=document.getElementById("btnentrada");


if(numero>0 ){
  for(var i=1;i<=numero;i++){
    rtas+="<div id='rta"+i+"'> </div>";
  }
  document.getElementById("rtas").innerHTML=rtas;


  document.getElementById("btnOpciones").innerHTML=btnOpciones;
   for(var i=1;i<=numero;i++){
        div="<div class='container mt-3 bg-warning rounded border border-danger'>";
        label="<label class='pl=5 mt-5'> Tamaño de pizza "+i+"</label>";
    select = "<select name='pizzas' id='pizzas'>";
    select+="<option value='pequeña'> pequeña </option>";
    select+="<option value='mediana'> mediana </option>";
    select+="<option value='grande'> grande </option>";
    select+="</select>";
    div+=label+select+"</div>";

    document.getElementById("rta"+i).innerHTML=div;


      }
  }
    else
    {
      div="<div class='container mt-3 bg-warning rounded border border-danger'>";
      div+="<h3>NO se aceptan pedidos negativos</h3></div>";
    document.getElementById("rtas").innerHTML=div;

    }
    document.getElementById("btnNuevo").innerHTML=btnsalida;
    espacioNumero.disabled  = true;
    btnNumero.disabled  = true;

  
}

function cargarOpciones(numeroPedidos)
{

  cambiarDePagina();
  var numpedidos="";
  for(var i=1;i<=numeroPedidos;i++){
    numpedidos+="<div id='opcion"+i+"'> </div>";
  }
  document.getElementById("opcionesPizza").innerHTML=numpedidos;
}

function cambiarDePagina()
{
  location.href="../pizzeria/html/opciones.html";
}


function leerPizzas(pizzas)
{

var msg="";
for(let i=0;i<pizzas.length;i++)
{

	msg+="<hr>"+pizzas[i].sabor+",<a href='"+pizzas[i].url_Imagen+"'>Ver imagen</a>";
	var precios=pizzas[i].precio;
	msg+=leerPrecios(precios);

}
return msg;
}

function leerPrecios(precios)
{

var msg="";
for(let i=0;i<precios.length;i++)
{

	msg+="<br> Tamaño:"+precios[i].tamano+", Precio:"+precios[i].precio;
	

}
return msg;
}


function leerAdicional(adicional)
{

var msg="<hr><br>Ingredientes adicionales</br>";
for(let i=0;i<adicional.length;i++)
{

	msg+="<br>"+adicional[i].nombre_ingrediente+","+adicional[i].valor;

}
return msg;
}

mostrar();