![Pizzería](http://www.madarme.co/portada-web.png)
# Título del proyecto:

#### Ejemplo de Lectura de datos JSON 
***
## Índice
1. [Características](#caracter-sticas-)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologías](#tecnologías)
4. [IDE](#ide)
5. [Instalación](#instalación)
6. [Demo](#demo)
7. [Autor(es)](#autores)
8. [Institución Académica](#institución-académica)
***

#### Características:

  - Simulacion de un sistema para el pedido de pizzas en un restaurante
  - Proyecto adaptado del proyecto base [Descargar proyecto base](https://www.dropbox.com/sh/a37tzh1q72b5hoq/AAA3UhT_3yNgBDKfA3J7Dgtya?dl=0)
  - Proyecto con lectura de datos json a través de la API fecth JavaScript
  - Carga dinámica del JSON 
  - Archivo json de ejemplo: [ver](https://raw.githubusercontent.com/madarme/persistencia/main/pizza.json)
  - CSS y paleta de colores definida por bootstrap con la tematica de pizzeria [Descargar Bootstrap](https://getbootstrap.com/docs/4.5/getting-started/download/)
***
  #### Contenido del proyecto
  - [index.html](https://gitlab.com/andreseduardogs/lectura-json-ii-2020-pizzeria/-/blob/master/index.html): Archivo principal de invocación a la lectura de JSON
  - [js/proceso.js](https://gitlab.com/andreseduardogs/lectura-json-ii-2020-pizzeria/-/blob/master/js/proceso.js): Archivo JS con el proceso de lectura del JSON y sus funciones adicionales para la impresión de resultados

***
#### Tecnologías

  - HTML5
  - JavaScript
  - CSS(definido por Bootstrap)

Usted puede ver el siguiente marco conceptual sobre la API fetch:

  - [Vídeo explicativo lectura con fetch()](https://www.youtube.com/watch?v=DP7Hkr2ss_I)
  - [Gúia de Mozzilla JSON](https://developer.mozilla.org/es/docs/Learn/JavaScript/Objects/JSON)
  
  ***
#### IDE

- El proyecto se desarrolla usando sublime text 3 
- Visor de JSON -(http://jsonviewer.stack.hu/)

***
### Instalación

  - Herramienta sublime text 3-> [descargar](https://www.sublimetext.com/3).
  - Firefox Devoloper Edition-> [descargar](https://www.mozilla.org/es-ES/firefox/developer/).
El software es necesario para ver la interacción por consola y depuración del código JS


```sh
-Descargar proyecto
-Invocar página index.html desde Firefox 
```

***
### Demo

 - Para ver el demo de la aplicación puede dirigirse a la pagina: [Pizzeria](http://ufps35.madarme.co/pizzeria/).
 - Una vez dentro de la pagina se encontrara el logo y el nombre del restaurante, junto con el nombre y codigo del estudiante que realizo el diseño de la pagina.
  - Se pedira ingresar la cantidad de pizzas que contiene el pedido y luego confirmar dicha cantidad a traves del boton mostrar.
  - Se pedira que seleccione un tamaño entre pequeño, mediano y grande para cada pizza del pedido por separado, para luego pasar a otra pagina donde se terminara de seleccionar el sabor y adiccionales.


***
### Autor(es)
  - Proyecto adaptado por el estudiante Andres Eduardo Garcia Sosa, representado por el codigo: 1151861(andreseduardogs@ufps.edu.co).
  - Proyecto base realizado por [Marco Adarme] (<madarme@ufps.edu.co>).


***
### Institución Académica   
Proyecto desarrollado en la Materia programación web(Grupo B) del  [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]


   [Marco Adarme]: <http://madarme.co>
   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>
   
